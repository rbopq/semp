#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include <wiringPi.h>
#include "fsm.h"

#define GPIO_BUTTON	2
#define GPIO_LED	3
#define GPIO_CUP	4
#define GPIO_COFFEE	5
#define GPIO_MILK	6

#define CUP_TIME	250
#define COFFEE_TIME	3000
#define MILK_TIME	3000

enum cofm_state {
	COFM_WAITING,
	COFM_CUP,
	COFM_COFFEE,
	COFM_MILK,
};

enum monedero_state {
	monedero_DINERO,
	monedero_DEVOLVER,
	monedero_ACUMULAR
};


static int button = 0;
//static void button_isr (void) { button = 1; }
//static int button_pressed (fsm_t* this) { return button; }

static int dinero_acc = 0;
static int dinero_int = 0;
static int dinero_val = 0;
static int dinero_dev = 0;
static int precio_cafe = 50;
static void acumulado(fsm_t* this) {
	dinero_acc = dinero_acc + dinero_val;
}

static int hay_moneda_nueva(fsm_t* this) {
	return dinero_int;
}


static int pulsar_boton_y_dinero_suficiente (fsm_t* this) { return button && (dinero_acc >= precio_cafe); }
static int sin_entrada (fsm_t* this) { return 1; }

static int timer = 0;
static void timer_isr (union sigval arg) { timer = 1; }
static void timer_start (int ms)
{
	timer_t timerid;
	struct itimerspec spec;
	struct sigevent se;
	se.sigev_notify = SIGEV_THREAD;
	se.sigev_value.sival_ptr = &timerid;
	se.sigev_notify_function = timer_isr;
	se.sigev_notify_attributes = NULL;
	spec.it_value.tv_sec = ms / 1000;
	spec.it_value.tv_nsec = (ms % 1000) * 1000000;
	spec.it_interval.tv_sec = 0;
	spec.it_interval.tv_nsec = 0;
	timer_create (CLOCK_REALTIME, &se, &timerid);
	timer_settime (timerid, 0, &spec, NULL);
	timer = 0;
}
static int timer_finished (fsm_t* this) { return timer; }


static void cup (fsm_t* this)
{
	digitalWrite (GPIO_LED, LOW);
	digitalWrite (GPIO_CUP, HIGH);
	timer_start (CUP_TIME);
	button = 0;
}

static void coffee (fsm_t* this)
{
	digitalWrite (GPIO_CUP, LOW);
	digitalWrite (GPIO_COFFEE, HIGH);
	timer_start (COFFEE_TIME);
}

static void milk (fsm_t* this)
{
	digitalWrite (GPIO_COFFEE, LOW);
	digitalWrite (GPIO_MILK, HIGH);
	timer_start (MILK_TIME);
}

static void finish (fsm_t* this)
{
	digitalWrite (GPIO_MILK, LOW);
	digitalWrite (GPIO_LED, HIGH);
}

static void dinero (fsm_t* this)
{

}

static void devolver (fsm_t* this)
{ 
	dinero_dev = dinero_acc - precio_cafe;
	dinero_acc =0;
	//printf("Dinero ACC = 0\n");
}


// Explicit FSM description
static fsm_trans_t cofm[] = {
	{ COFM_WAITING,  pulsar_boton_y_dinero_suficiente, COFM_CUP,     cup    },
	{ COFM_CUP,     timer_finished, COFM_COFFEE,  coffee },
	{ COFM_COFFEE,  timer_finished, COFM_MILK,    milk   },
	{ COFM_MILK,    timer_finished, COFM_WAITING, finish },
	{-1, NULL, -1, NULL },
};


static fsm_trans_t monedero[] = {
	{ monedero_DINERO, pulsar_boton_y_dinero_suficiente, monedero_DEVOLVER, dinero },
	{ monedero_DEVOLVER, sin_entrada, monedero_DINERO, devolver },
	{ monedero_DINERO, hay_moneda_nueva, monedero_ACUMULAR, acumulado },
	{ monedero_ACUMULAR, sin_entrada, monedero_DINERO, NULL },
	{-1, NULL, -1, NULL },
};


// Utility functions, should be elsewhere

// res = a - b
	void
timeval_sub (struct timespec *res, struct timespec *a, struct timespec *b)
{
	printf("sub");
	res->tv_sec = a->tv_sec - b->tv_sec;
	res->tv_nsec = a->tv_nsec - b->tv_nsec;
	if (res->tv_nsec < 0) {
		--res->tv_sec;
		res->tv_nsec += 1000000000;
	}
}

// res = a + b
	void
timeval_add (struct timespec *res, struct timespec *a, struct timespec *b)
{
	printf("add");
	res->tv_sec = a->tv_sec + b->tv_sec
		+ a->tv_nsec / 1000000000 + b->tv_nsec / 1000000000; 
	res->tv_nsec = a->tv_nsec % 1000000000 + b->tv_nsec % 1000000000;
}

// wait until next_activation (absolute time)
void delay_until (struct timespec * next_activation)
{
  struct timespec now, timeout;
  clock_gettime(CLOCK_REALTIME, &now);
  timeval_sub (&timeout, next_activation, &now);
  pselect (0, NULL, NULL, NULL, &timeout, NULL);
}




int main ()
{
	struct timespec start, stop;
	struct timespec clk_period = { 0, 250 * 1000000 };
	struct timespec next_activation;
	fsm_t* cofm_fsm = fsm_new (cofm);
	fsm_t* monedero_fsm = fsm_new (monedero);
	double tex_mon_last, tex_caf_last; // Últimos tiempos de ejecución
	double tex_mon_max; // Tiempo máximo de ejecución monedero
	double tex_mon_0_1, tex_mon_0_2, tex_mon_1_0, tex_mon_2_0; // Tiempos de transición entre estados del monedero
	double tex_caf_max; // Tiempo máximo de ejecución café
	double tex_caf_0_1, tex_caf_1_2, tex_caf_2_3, tex_caf_3_0; // Tiempos de transición entre estados de la máquina de café

	int state_1, state_2; 

	wiringPiSetup();
	pinMode (GPIO_BUTTON, INPUT);
	wiringPiISR (GPIO_BUTTON, INT_EDGE_FALLING, button_isr);
	pinMode (GPIO_CUP, OUTPUT);
	pinMode (GPIO_COFFEE, OUTPUT);
	pinMode (GPIO_MILK, OUTPUT);
	pinMode (GPIO_LED, OUTPUT);
	digitalWrite (GPIO_LED, HIGH);

	clock_gettime (CLOCK_REALTIME, &next_activation);
	tex_mon_last=0;
	tex_caf_last=0;
	tex_mon_0_1=0;
	tex_mon_1_0=0;
	tex_mon_0_2=0;
	tex_mon_2_0=0;
	tex_caf_0_1=0;
	tex_caf_1_2=0;
	tex_caf_2_3=0;
	tex_caf_3_0=0;

	int i;
	for(i=0; i <1000; i++){

		button=1;
		timer=1;
		dinero_int=50; 
		dinero_val=50;

		state_1=monedero_fsm->current_state;
		clock_gettime( CLOCK_REALTIME, &start);
		fsm_fire (monedero_fsm);
		clock_gettime( CLOCK_REALTIME, &stop);	
		state_2=monedero_fsm->current_state;

		tex_mon_last = ( stop.tv_sec - start.tv_sec )+( stop.tv_nsec - start.tv_nsec );

		switch(state_1)
		{
			case 0: 
				if(state_2==1){
					if(tex_mon_last>tex_mon_0_1)
						tex_mon_0_1=tex_mon_last;
				}
				else{
					if(tex_mon_last>tex_mon_0_2)
						tex_mon_0_2=tex_mon_last;		
				}
				break;

			case 1:
				if(state_2==0){
					if(tex_mon_last>tex_mon_1_0)
						tex_mon_1_0=tex_mon_last;		
				}

				break;
			case 2: 
				if(state_2==0){

					if(tex_mon_last>tex_mon_2_0)
						tex_mon_2_0=tex_mon_last;		
				}			
				break;
		}

		state_1=cofm_fsm->current_state;
		clock_gettime( CLOCK_REALTIME, &start);
		fsm_fire (cofm_fsm);
		clock_gettime( CLOCK_REALTIME, &stop);	
		state_2=cofm_fsm->current_state;

		tex_caf_last = ( stop.tv_sec - start.tv_sec )+( stop.tv_nsec - start.tv_nsec );

		switch(state_1)
		{
			case 0: 
				if(state_2==1){
					if(tex_caf_last>tex_caf_0_1)
						tex_caf_0_1=tex_caf_last;
				}
				break;
			case 1:
				if(state_2==2){
					if(tex_caf_last>tex_caf_1_2)
						tex_caf_1_2=tex_caf_last;		
				}
				break;
			case 2: 
				if(state_2==3){
					if(tex_caf_last>tex_caf_2_3)
						tex_caf_2_3=tex_caf_last;		
				}			
				break;
			case 3: 
				if(state_2==0){
					if(tex_caf_last>tex_caf_3_0)
						tex_caf_3_0=tex_caf_last;		
				}			
				break;

		}
		printf("++++++++++TIEMPOS++++++++++\n");
		printf("MAQUINA CAFE: \n");
		printf( "0->1: %lf 1->2: %lf 2->3: %lf 3->0: %lf\n", tex_caf_0_1, tex_caf_1_2, tex_caf_2_3, tex_caf_3_0);
		printf( "0->1->2->3->0: %lf\n", tex_caf_0_1+tex_caf_1_2+tex_caf_2_3+tex_caf_3_0);
		printf("MAQUINA MONEDERO: \n");
		printf( "0->1: %lf 1->0: %lf 0->2: %lf 2->0: %lf\n", tex_mon_0_1, tex_mon_1_0, tex_mon_0_2, tex_mon_2_0);
		printf( "0->1->0: %lf 0->2->0: %lf\n", tex_mon_0_1+tex_mon_1_0, tex_mon_0_2+tex_mon_2_0);
		printf("\r\r\r\r");
		timeval_add (&next_activation, &next_activation, &clk_period);
		delay_until (&next_activation);
	}

	//}
}
