#include <stdlib.h>
#include <stdio.h>
#include "fsm.h"
#include <time.h>
#define BILLION  1000000000L;

fsm_t*
fsm_new (fsm_trans_t* tt)
{
  fsm_t* this = (fsm_t*) malloc (sizeof (fsm_t));
  fsm_init (this, tt);
  return this;
}

void
fsm_init (fsm_t* this, fsm_trans_t* tt)
{
  this->tt = tt;
  this->current_state = tt[0].orig_state;
}

void
fsm_fire (fsm_t* this)
{
  //struct timespec start, stop;
  //double accum;
  fsm_trans_t* t;

  for (t = this->tt; t->orig_state >= 0; ++t) {
  //clock_gettime( CLOCK_REALTIME, &start);
     if ((this->current_state == t->orig_state) && t->in(this)) {
      this->current_state = t->dest_state;
      if (t->out){
        t->out(this);
      }
   /*   clock_gettime( CLOCK_REALTIME, &stop);	
      accum = ( stop.tv_sec - start.tv_sec )
          + ( stop.tv_nsec - start.tv_nsec );
      printf("%lld.%.9ld\n", (long long)start.tv_sec, start.tv_nsec);
      printf("%lld.%.9ld\n", (long long)stop.tv_sec, stop.tv_nsec);
      printf( "%lf\n", accum );*/
      break;
    }
  }
}

