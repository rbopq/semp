#include <stdio.h>
#include <sys/time.h>
#include <sys/select.h>
#include <time.h>
#include <signal.h>
#include "tiempo.h"


extern int timer;

static void timer_isr (union sigval arg) 
{ 
	printf("timer_isr\n");
	timer = 1; 
}

void timer_start (int ms)
{
  timer_t timerid;
  struct itimerspec spec;
  struct sigevent se;
  se.sigev_notify = SIGEV_THREAD;
  se.sigev_value.sival_ptr = &timerid;
  se.sigev_notify_function = timer_isr;
  se.sigev_notify_attributes = NULL;
  spec.it_value.tv_sec = ms / 1000;
  spec.it_value.tv_nsec = (ms % 1000) * 1000000;
  spec.it_interval.tv_sec = 0;
  spec.it_interval.tv_nsec = 0;
  timer_create (CLOCK_REALTIME, &se, &timerid);
  timer_settime (timerid, 0, &spec, NULL);
  timer=0;
  
}


