#include <stdio.h>
#include <pthread.h>

#include "task.h"
#include <wiringPi.h>
#include "fsm.h"
#include "cafe.h"
#include "monedero.h"

fsm_t* cofm_fsm;
fsm_t* monedero_fsm;
pthread_mutex_t mutex_dinero;
int button =0;
int dinero_int=0;
int dinero_val=0;
int timer=0;

static
void *
task_cafe_func (void* arg)
{

  struct timeval next_activation;
  struct timeval now, timeout;
  

  cofm_fsm = fsm_cafe_new ();
   
  gettimeofday (&next_activation, NULL);
  while (1) {
	while (button ==0){
		
		scanf("%d %d %d", &dinero_int, &dinero_val, &button);
		
	}  
    struct timeval *period = task_get_period (pthread_self());
    timeval_add (&next_activation, &next_activation, period);
    gettimeofday (&now, NULL);
    timeval_sub (&timeout, &next_activation, &now);
    select (0, NULL, NULL, NULL, &timeout) ;
	fsm_fire (cofm_fsm);
  }
  return 0;
}

static
void *
task_monedero_func (void* arg)
{

  struct timeval next_activation;
  struct timeval now, timeout;
  
  monedero_fsm = fsm_monedero_new ();
  
  gettimeofday (&next_activation, NULL);
  while (1) {
    struct timeval *period = task_get_period (pthread_self());
    timeval_add (&next_activation, &next_activation, period);
    gettimeofday (&now, NULL);
    timeval_sub (&timeout, &next_activation, &now);
    select (0, NULL, NULL, NULL, &timeout) ;
	fsm_fire (monedero_fsm);
  }
  return 0;
}


int
main ()
{ 


	
	mutex_init(&mutex_dinero, 1);
	pthread_t t_monedero = task_new ("monedero", task_monedero_func, 250, 250, 1, 1024);
	pthread_t t_cafe = task_new ("cafe", task_cafe_func, 250, 250, 2, 1024);
	
	pthread_join (t_monedero, NULL);
	pthread_join (t_cafe, NULL);
		

	
  return 0;
}
