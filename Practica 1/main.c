/*
 * main.c
 *
 *  Created on: Oct 20, 2016
 *  Author: Rodolfo Boris Oporto Quisbert (basado en ejemplo de JMMoya)
 */

#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include <wiringPi.h>
#include "fsm.h"

/* Definici�n de los pines de salida y entrada de las FSM*/
#define GPIO_BTN_CAFE 2     // Bot�n para pedir un caf�
#define GPIO_BTN_CANCELAR 3 // Bot�n para cancelar

#define GPIO_LED	14 // LED que avisa si la m�quina est� lista para otro caf�

#define GPIO_VASO	15 // Se�al que simula el vaso
#define GPIO_CAFE	18 // Se�al que simula el caf�
#define GPIO_LECHE	23 // Se�al que simula la leche

#define GPIO_50_IN_COIN 6 // Se�al que simula la introducci�n de 50 cts
#define GPIO_20_IN_COIN 13 // Se�al que simula la introducci�n de 20 cts
#define GPIO_10_IN_COIN 29// Se�al que simula la introducci�n de 10 cts
#define GPIO_5_IN_COIN  26// Se�al que simula la introducci�n de 5 cts

#define GPIO_50_OUT_COIN 12 // Se�al que simula la devoluci�n de 50 cts
#define GPIO_20_OUT_COIN 16// Se�al que simula la devoluci�n de 20 cts
#define GPIO_10_OUT_COIN 20// Se�al que simula la devoluci�n de 10 cts
#define GPIO_5_OUT_COIN  21// Se�al que simula la devoluci�n de 5 cts

/* Definici�n de los tiempos en milisegundos*/
#define CUP_TIME	25   // Tiempo para el vaso
#define COFFEE_TIME	300  // Tiempo de espera para el caf�
#define MILK_TIME	300  // Tiempo de espera para la leche
#define COIN_TIME	100  // Tiempo de espera para pulso de devoluci�n moneda

/* Tipos enumerados que representan los estados de las dos FSM*/
enum estado_cafe
{
  CFSM_INICIO, CFSM_COBRAR, CFSM_VASO, CFSM_CAFE, CFSM_LECHE, CFSM_DEVOLVER,
};

enum estado_monedero
{
  MFSM_INICIO, MFSM_MONEDA, MFSM_COBRAR, MFSM_DEVOLVER, MFSM_DEVOLVIENDO,
};

static int money = 0;    // Variable que representa el dinero
static int button_cafe = 0; // Variable que representa el bot�n del caf�
static int button_devolver = 0; // Variable que representa el bot�n de cancelar
static int signal_devolver = 0; // Variable que representa la se�al de devoluci�n enviada desde la fsm caf� al monedero
static int coin50_in = 0; // Variable que representa el bot�n de moneda de 50 cts
static int coin20_in = 0; // Variable que representa el bot�n de moneda de 20 cts
static int coin10_in = 0; // Variable que representa el bot�n de moneda de 10 cts
static int coin5_in = 0; // Variable que representa el bot�n de moneda de 5 cts
static int cobrar = 0; // Variable que representa la se�al de cobrar para el monedero

static void
button_cafe_isr (void)
{
  button_cafe = 1;
}
static void
button_cancelar_isr (void)
{
  button_devolver = 1;
}
static void
coin_50_isr (void)
{
  coin50_in = 1;
}
static void
coin_20_isr (void)
{
  coin20_in = 1;
}
static void
coin_10_isr (void)
{
  coin10_in = 1;
}
static void
coin_5_isr (void)
{
  coin5_in = 1;
}

static int
button_cafe_pressed (fsm_t* this)
{
  if (money >= 50)
    return button_cafe;
  else
    return 0;
}

static int
button_devolver_pressed (fsm_t* this)
{
  return button_devolver;
}

static int
devolver_triggered (fsm_t* this)
{
  return signal_devolver;
}

static int timer = 0;

static void
timer_isr (union sigval arg)
{
  timer = 1;
}
static void
timer_start (int ms)
{
  timer_t timerid;
  struct itimerspec spec;
  struct sigevent se;
  se.sigev_notify = SIGEV_THREAD;
  se.sigev_value.sival_ptr = &timerid;
  se.sigev_notify_function = timer_isr;
  se.sigev_notify_attributes = NULL;
  spec.it_value.tv_sec = ms / 1000;
  spec.it_value.tv_nsec = (ms % 1000) * 1000000;
  spec.it_interval.tv_sec = 0;
  spec.it_interval.tv_nsec = 0;
  timer_create (CLOCK_REALTIME, &se, &timerid);
  timer_settime (timerid, 0, &spec, NULL);
  timer = 0;
}
static int
timer_finished (fsm_t* this)
{
  return timer;
}
static int
coin_introduced (fsm_t* this)
{
  if (coin50_in == 1 || coin20_in == 1 || coin10_in == 1 || coin5_in == 1)
    return 1;
  else
    return 0;
}
static int
cobrar_triggered (fsm_t* this)
{
  return cobrar;
}
static int
wallet_not_empty (fsm_t* this)
{
  if (money > 0)
    return 1;
  else
    return 0;
}
static int
wallet_empty (fsm_t* this)
{
  if (money == 0)
    return 1;
  else
    return 0;
}

static int
aux_true (fsm_t* this)
{
  return 1;
}
/*Funciones que realizan las acciones */
static void
cfsm_inicio (fsm_t* this)
{
  printf("\n\rGPIO_LED ->1\n\r");
  digitalWrite (GPIO_LED, HIGH);
  button_devolver = 0;
}

static void
cfsm_cobrar (fsm_t* this)
{
  printf("\n\rGPIO_LED ->0\n\r");
  digitalWrite (GPIO_LED, LOW);
  cobrar = 1;
  button_cafe = 0;
}

static void
cfsm_vaso (fsm_t* this)
{
  printf("\n\r GPIO_VASO ->1\n\r");
  digitalWrite (GPIO_VASO, HIGH);
  timer_start (CUP_TIME);
  cobrar = 0;
}

static void
cfsm_cafe (fsm_t* this)
{
  printf("\n\r GPIO_VASO ->0\n\r");
  printf("\n\r GPIO_CAFE ->1\n\r");
  digitalWrite (GPIO_VASO, LOW);
  digitalWrite (GPIO_CAFE, HIGH);
  timer_start (COFFEE_TIME);
}

static void
cfsm_leche (fsm_t* this)
{
  printf("\n\r GPIO_CAFE ->0\n\r");
  printf("\n\r GPIO_LECHE ->1\n\r");
  digitalWrite (GPIO_CAFE, LOW);
  digitalWrite (GPIO_LECHE, HIGH);
  timer_start (MILK_TIME);
}

static void
cfsm_devolver (fsm_t* this)
{
  printf("\n\r GPIO_LECHE ->0\n\r");
  digitalWrite (GPIO_LECHE, LOW);
  signal_devolver = 1;
}

static void
cfsm_devolver_2 (fsm_t* this)
{
  printf("\n\r GPIO_LED ->0\n\r");
  digitalWrite (GPIO_LED, LOW);
  signal_devolver = 1;
}

static void
mfsm_inicio (fsm_t* this)
{

}

static void
mfsm_moneda (fsm_t* this)
{
  if (coin50_in)
    {
      money = money + 50;
      coin50_in = 0;
      printf("\n\r Dinero = %d \n\r", money);
    }
  else if (coin20_in)
    {
      money = money + 20;
      coin20_in = 0;
      printf("\n\r Dinero = %d \n\r", money);
    }
  else if (coin10_in)
    {
      money = money + 10;
      coin10_in = 0;
      printf("\n\r Dinero = %d \n\r", money);
    }
  else if (coin5_in)
    {
      money = money + 5;
      coin5_in = 0;
      printf("\n\r Dinero = %d \n\r", money);
    }
}

static void
mfsm_cobrar (fsm_t* this)
{
  money = money - 50;
  printf("\n\r Dinero = %d \n\r", money);
  cobrar = 0;
}

static void
mfsm_devolver (fsm_t* this)
{
  printf("\n\r GPIO_50_OUT_COIN ->0\n\r");
  printf("\n\r GPIO_20_OUT_COIN ->0\n\r");
  printf("\n\r GPIO_10_OUT_COIN ->0\n\r");
  printf("\n\r GPIO_5_OUT_COIN ->0\n\r");
  digitalWrite (GPIO_50_OUT_COIN, LOW);
  digitalWrite (GPIO_20_OUT_COIN, LOW);
  digitalWrite (GPIO_10_OUT_COIN, LOW);
  digitalWrite (GPIO_5_OUT_COIN, LOW);
}

static void
mfsm_devolviendo (fsm_t* this)
{
  if (money > 50)
    {
      money = money - 50;
      digitalWrite (GPIO_50_OUT_COIN, HIGH);
      printf("\n\r GPIO_50_OUT_COIN ->1\n\r");
      printf("\n\r Dinero = %d \n\r", money);
    }
  else if (money < 50 & money >= 20)
    {
      money = money - 20;
      digitalWrite (GPIO_20_OUT_COIN, HIGH);
      printf("\n\r GPIO_20_OUT_COIN ->1\n\r");
      printf("\n\r Dinero = %d \n\r", money);
    }
  else if (money < 20 & money >= 10)
    {
      money = money - 10;
      digitalWrite (GPIO_10_OUT_COIN, HIGH);
      printf("\n\r GPIO_10_OUT_COIN ->1\n\r");
      printf("\n\r Dinero = %d \n\r", money);
    }
  else if (money < 10)
    {
      money = money - 5;
      digitalWrite (GPIO_5_OUT_COIN, HIGH);
      printf("\n\r GPIO_5_OUT_COIN ->1\n\r");
      printf("\n\r Dinero = %d \n\r", money);
    }
  timer_start (COIN_TIME);
}

/*Descripci� de las transiciones de la m�quina de caf�*/
static fsm_trans_t cfsm[] =
  {
    { CFSM_INICIO, button_cafe_pressed, CFSM_COBRAR, cfsm_cobrar },
    { CFSM_COBRAR, aux_true, CFSM_VASO, cfsm_vaso },
    { CFSM_VASO, timer_finished, CFSM_CAFE, cfsm_cafe },
    { CFSM_CAFE, timer_finished, CFSM_LECHE, cfsm_leche },
    { CFSM_LECHE, timer_finished, CFSM_DEVOLVER, cfsm_devolver },
    { CFSM_INICIO, button_devolver_pressed, CFSM_DEVOLVER, cfsm_devolver_2 },
    { CFSM_DEVOLVER, aux_true, CFSM_INICIO, cfsm_inicio },
    { -1, NULL, -1, NULL }, };

/*Descripci� de las transiciones de la m�quina monedero*/
static fsm_trans_t mfsm[] =
  {
    { MFSM_INICIO, coin_introduced, MFSM_MONEDA, mfsm_moneda },
    { MFSM_MONEDA, aux_true, MFSM_INICIO, mfsm_inicio },
    { MFSM_INICIO, cobrar_triggered, MFSM_COBRAR, mfsm_cobrar },
    { MFSM_COBRAR, aux_true, MFSM_INICIO, mfsm_inicio },
    { MFSM_INICIO, devolver_triggered, MFSM_DEVOLVER, mfsm_inicio },
    { MFSM_DEVOLVER, wallet_not_empty, MFSM_DEVOLVIENDO, mfsm_devolviendo },
    { MFSM_DEVOLVIENDO, timer_finished, MFSM_DEVOLVER, mfsm_devolver },
    { MFSM_DEVOLVER, wallet_empty, MFSM_INICIO, mfsm_inicio },
    { -1, NULL, -1, NULL }, };
// Utility functions, should be elsewheres
// res = a - b
void
timeval_sub (struct timeval *res, struct timeval *a, struct timeval *b)
{
  res->tv_sec = a->tv_sec - b->tv_sec;
  res->tv_usec = a->tv_usec - b->tv_usec;
  if (res->tv_usec < 0)
    {
      --res->tv_sec;
      res->tv_usec += 1000000;
    }
}

// res = a + b
void
timeval_add (struct timeval *res, struct timeval *a, struct timeval *b)
{
  res->tv_sec = a->tv_sec + b->tv_sec + a->tv_usec / 1000000
      + b->tv_usec / 1000000;
  res->tv_usec = a->tv_usec % 1000000 + b->tv_usec % 1000000;
}

// wait until next_activation (absolute time)
void
delay_until (struct timeval* next_activation)
{
  struct timeval now, timeout;
  gettimeofday (&now, NULL);
  timeval_sub (&timeout, next_activation, &now);
  select (0, NULL, NULL, NULL, &timeout);
}

int
main ()
{
  struct timeval clk_period =
    { 0, 250 * 1000 };
  struct timeval next_activation;
  fsm_t* cfsm_fsm = fsm_new (cfsm);
  fsm_t* mfsm_fsm = fsm_new (mfsm);

  wiringPiSetup ();
  /* Definici�n de las entradas del sistema*/
  pinMode (GPIO_BTN_CAFE, INPUT);
  wiringPiISR (GPIO_BTN_CAFE, INT_EDGE_FALLING, button_cafe_isr);
  pinMode (GPIO_BTN_CANCELAR, INPUT);
  wiringPiISR (GPIO_BTN_CANCELAR, INT_EDGE_FALLING, button_cancelar_isr);
  pinMode (GPIO_50_IN_COIN, INPUT);
  wiringPiISR (GPIO_50_IN_COIN, INT_EDGE_FALLING, coin_50_isr);
  pinMode (GPIO_20_IN_COIN, INPUT);
  wiringPiISR (GPIO_20_IN_COIN, INT_EDGE_FALLING, coin_20_isr);
  pinMode (GPIO_10_IN_COIN, INPUT);
  wiringPiISR (GPIO_10_IN_COIN, INT_EDGE_FALLING, coin_10_isr);
  pinMode (GPIO_5_IN_COIN, INPUT);
  wiringPiISR (GPIO_5_IN_COIN, INT_EDGE_FALLING, coin_5_isr);

  /* Definici�n de las salidas del sistema*/
  pinMode (GPIO_VASO, OUTPUT);
  pinMode (GPIO_CAFE, OUTPUT);
  pinMode (GPIO_LECHE, OUTPUT);
  pinMode (GPIO_LED, OUTPUT);
  pinMode (GPIO_50_OUT_COIN, OUTPUT);
  pinMode (GPIO_20_OUT_COIN, OUTPUT);
  pinMode (GPIO_10_OUT_COIN, OUTPUT);
  pinMode (GPIO_5_OUT_COIN, OUTPUT);

  digitalWrite (GPIO_LED, HIGH);
  digitalWrite (GPIO_VASO, LOW);
  digitalWrite (GPIO_CAFE, LOW);
  digitalWrite (GPIO_LECHE, LOW);
  digitalWrite (GPIO_50_OUT_COIN, LOW);
  digitalWrite (GPIO_20_OUT_COIN, LOW);
  digitalWrite (GPIO_10_OUT_COIN, LOW);
  digitalWrite (GPIO_5_OUT_COIN, LOW);

  gettimeofday (&next_activation, NULL);
  while (scanf ("%d %d %d %d %d %d %d", &button_cafe, &button_devolver,
		&coin50_in, &coin20_in, &coin10_in, &coin5_in, &timer) == 7)
    {
      fsm_fire (cfsm_fsm); // Lanzamos el aut�mata de la maquina de cafe
      fsm_fire (mfsm_fsm); // Lanzamos el aut�mata del moneder
      timeval_add (&next_activation, &next_activation, &clk_period);
      delay_until (&next_activation);
    }
}

