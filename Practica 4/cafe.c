/*
 * fsm.c
 *
 *  Created on: Oct 20, 2016
 *      Author: JMMoya
 */


#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/time.h>
#include <wiringPi.h>
#include "fsm.h"
#include "cafe.h"
#include "tiempo.h"

#define GPIO_BUTTON	11
#define GPIO_LED	2
#define GPIO_CUP	4
#define GPIO_COFFEE	17
#define GPIO_MILK	27

#define CUP_TIME	250
#define COFFEE_TIME	3000
#define MILK_TIME	3000


/* Definición de estados*/
enum cofm_state {
  COFM_WAITING,
  COFM_CUP,
  COFM_COFFEE,
  COFM_MILK,
};


extern int button;
static void button_isr (void) { button = 1; }
//static int button_pressed (fsm_t* this) { return button; }

extern int timer;
extern int dinero_acc;
extern int precio_cafe;
/* Funciones que activan las transiciones */

static int pulsar_boton_y_dinero_suficiente (fsm_t* this) 
{ 
	return (button && (dinero_acc >= precio_cafe));
	
}

static int timer_finished (fsm_t* this) 
{ 
	return timer; 
}


/* Funciones que realizan las acciones */
static void cup (fsm_t* this)
{
  digitalWrite (GPIO_LED, LOW);
  digitalWrite (GPIO_CUP, HIGH);
  timer_start (CUP_TIME);
  printf("CUP\n");
  
  
}

static void coffee (fsm_t* this)
{
  digitalWrite (GPIO_CUP, LOW);
  digitalWrite (GPIO_COFFEE, HIGH);
  timer_start (COFFEE_TIME);
  printf("COFFE\n");

}

static void milk (fsm_t* this)
{
  digitalWrite (GPIO_COFFEE, LOW);
  digitalWrite (GPIO_MILK, HIGH);
  timer_start (MILK_TIME);
  printf("MILK\n");

}

static void finish (fsm_t* this)
{
  digitalWrite (GPIO_MILK, LOW);
  digitalWrite (GPIO_LED, HIGH);
  printf("FINISH\n");
  button = 0;

}

/* Tabla de transiciónes */
static fsm_trans_t cofm[] = {
  { COFM_WAITING,  pulsar_boton_y_dinero_suficiente, COFM_CUP,     cup    },
  { COFM_CUP,     timer_finished, COFM_COFFEE,  coffee },
  { COFM_COFFEE,  timer_finished, COFM_MILK,    milk   },
  { COFM_MILK,    timer_finished, COFM_WAITING, finish },
  {-1, NULL, -1, NULL },
};


fsm_t*
fsm_cafe_new (void)
{
  fsm_t* this = (fsm_t*) malloc (sizeof (fsm_t));
  fsm_cafe_init (this);
  return this;
}

void
fsm_cafe_init (fsm_t* this)
{
  this->tt = cofm;
  this->current_state = cofm[0].orig_state;
  
  wiringPiSetupGpio();
  pinMode (GPIO_BUTTON, INPUT);
  wiringPiISR (GPIO_BUTTON, INT_EDGE_FALLING, button_isr);
  pinMode (GPIO_CUP, OUTPUT);
  pinMode (GPIO_COFFEE, OUTPUT);
  pinMode (GPIO_MILK, OUTPUT);
  pinMode (GPIO_LED, OUTPUT);
  digitalWrite (GPIO_LED, HIGH);
  
}

