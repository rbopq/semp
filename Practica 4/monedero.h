/*
 * monedero.h
 *
 *  Created on: Dec 1, 2016
 *  Author: Rodolfo B. Oporto Quisbert
 */

#ifndef MONEDERO_H_
#define MONEDERO_H_

fsm_t* fsm_monedero_new (void);
void fsm_monedero_init (fsm_t* this);

#endif /* MONEDERO_H_ */
