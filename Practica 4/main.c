#include <stdio.h>

#include "reactor.h"
#include <wiringPi.h>
#include "fsm.h"
#include "cafe.h"
#include "monedero.h"

fsm_t* cofm_fsm;
fsm_t* monedero_fsm;

int button =0;
int timer =0;
int dinero_int=0;
int dinero_val=0;

static
void
task_cafe_func (struct event_handler_t* this)
{
  static const struct timeval period = { 0, 250*1000 };
  // Funcionalidad
  fsm_fire (cofm_fsm);
		
  timeval_add (&this->next_activation, &this->next_activation, &period);
}

static
void
task_monedero_func (struct event_handler_t* this)
{
  static const struct timeval period = { 0, 250*1000 };
  // Funcionalidad
  fsm_fire (monedero_fsm);
		
  timeval_add (&this->next_activation, &this->next_activation, &period);
}


int
main ()
{ 
  
  cofm_fsm=fsm_cafe_new ();
  monedero_fsm=fsm_monedero_new ();

  EventHandler eh_cafe;
  EventHandler eh_monedero;

  reactor_init ();

  event_handler_init (&eh_cafe, 1, task_cafe_func);
  reactor_add_handler (&eh_cafe);
  
  event_handler_init (&eh_monedero, 2, task_monedero_func);
  reactor_add_handler (&eh_monedero);
  scanf("%d %d", &dinero_int, &dinero_val);
  while (1) {
    reactor_handle_events ();
  }
  
  return 0;
}
