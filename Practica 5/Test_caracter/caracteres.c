/*
* caracteres.c
*
*  Created on: 	Ene 20, 2017
*  Authors: 		Rodolfo B. Oporto Quisbert/ Yousra Hasnaoui
*  Description: 	Este fichero implementa las funciones necesarias  para la manipulación de carracteres 
*					para su representación en un array de LED para la práctica de SEMP 2016
*/

#include <string.h>
#include "caracteres.h"

/* Descripción de los caracteres como arrays de 8 filas y 6 columnas*/

static char c_0 [6]={
	0b01111100,
	0b10100010,
	0b10010010,
	0b10001010,
	0b01111100,
	0b00000000
};

static char c_1 [6]={
	0b00000000,
	0b10000100,
	0b11111110,
	0b10000000,
	0b00000000,
	0b00000000
};

static char c_2 [6] ={
	0b10000100,
	0b11000010,
	0b10100010,
	0b10010010,
	0b10001100,
	0b00000000
};

static char c_3 [6]={
	0b01000010,
	0b10000010,
	0b10001010,
	0b10010110,
	0b01100010,
	0b00000000
};

static char c_4 [6]={
	0b00110000,
	0b00101000,
	0b00100100,
	0b11111110,
	0b00100000,
	0b00000000
};

static char c_5 [6]={
	0b01001110,
	0b10001010,
	0b10001010,
	0b10001010,
	0b01110010,
	0b00000000
};

static char c_6 [6]={
	0b01111000,
	0b10010100,
	0b10010010,
	0b10010010,
	0b01100000,
	0b00000000
};

static char c_7 [6]={
	0b00000110,
	0b00000010,
	0b11100010,
	0b00010010,
	0b00001110,
	0b00000000
};

static char c_8 [6]={
	0b01101100,
	0b10010010,
	0b10010010,
	0b10010010,
	0b01101100,
	0b00000000
};

static char c_9 [6]={
	0b00001100,
	0b10010010,
	0b10010010,
	0b01010010,
	0b00111100,
	0b00000000
};

static char c_dp [6]={
	0b00000000,
	0b01101100,
	0b01101100,
	0b00000000,
	0b00000000,
	0b00000000
};

static char c_X [6]={
	0b11000110,
	0b00101000,
	0b00010000,
	0b00101000,
	0b11000110,
	0b00000000
};


void time_to_array(char * pantalla, char h1, char h0, char m1, char m0)
{
	memcpy(&pantalla[0] ,(char*) get_caracter(h1) , 6);
	memcpy(&pantalla[6] ,(char*) get_caracter(h0) , 6);
	memcpy(&pantalla[12],(char*) get_caracter(':'), 6);
	memcpy(&pantalla[18],(char*) get_caracter(m1) , 6);
	memcpy(&pantalla[24],(char*) get_caracter(m0) , 6);
		
}

char* get_caracter(char character)
{
	switch(character)
	{
		case '0':
			return c_0;
		break;
		case '1':
			return c_1;
		break;
		case '2':
			return c_2;
		break;
		case '3':
			return c_3;
		break;
		case '4':
			return c_4;
		break;
		case '5':
			return c_5;
		break;
		case '6':
			return c_6;
		break;
		case '7':
			return c_7;
		break;
		case '8':
			return c_8;
		break;
		case '9':
			return c_9;
		break;
		case ':':
			return c_dp;
		break;
		case 'X':
			return c_X;
		break;
		default:
			return 0;
		break;
		
	}
}

