/*
*  main.c
*
*  Created on: 		Ene 20, 2017
*  Authors: 		Rodolfo B. Oporto Quisbert/ Yousra Hasnaoui
*  Description: 	Este fichero es un fichero de ejemplo de la librería "caracteres"
*/

#include <stdio.h>
#include <string.h>
#include "caracteres.h"

/* Función auxiliar para representar chars en formato binario para imprimirlos por pantalla*/
const char *byte_to_binary(char x)
{
    static char b[8];
    b[0] = '\0';

    char z;
    for (z = 0x80; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

int main ()
{
	char pantalla[30]; // Pantalla 8x30 (5 caracteres)
	time_to_array(pantalla, '1', '8', '0', '1'); // Hora 18:01
	
	/* Impresión del contenido de la pantalla en formato binario*/
	int i;
	for(i=0; i<30; i++)
	{
		printf("%s\n", byte_to_binary(pantalla[i]));
	}
	return 0;
}

