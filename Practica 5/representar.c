/*
*  representar.c
*
*  Created on: 		Ene 20, 2017
*  Authors: 		Rodolfo B. Oporto Quisbert/ Yousra Hasnaoui
*  Description: 	Este fichero implementa las funciones necesarias de la máquina de estados
*					representar para la Práctica 5 de SEMP-MUISE 2016
*/

#include "fsm.h"
#include "representar.h"

#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>
#include <wiringPi.h>

/* Correspondencia de los GPIO con los LED's asociados al display y a la barrera de infrarrojos*/
#define GPIO_LED_1	2
#define GPIO_LED_2	3
#define GPIO_LED_3	4
#define GPIO_LED_4	17
#define GPIO_LED_5	27
#define GPIO_LED_6	22
#define GPIO_LED_7	10
#define GPIO_LED_8	9
#define GPIO_IR 	11

/* Variable compartida que representa la pantalla de LED's a representar */
extern char pantalla[30]; 

/* Variable compartida que representa el fin del proceso de representación para que se comience la otra tarea */
extern int fin_representado; 

/* Variable que representa la barrera de infrarrojos */
extern int barrera;

/* Variable que representa la columna que se está representando en este isntante */
static int columna = 0;

/* Timer para las columnas */

static int timer_led = 0;
static void timer_isr (union sigval arg) 
{ 
	timer_led = 1;
	
}


static void timer_start (int ns)
{
  timer_t timerid;
  struct itimerspec spec;
  struct sigevent se;
  se.sigev_notify = SIGEV_THREAD;
  se.sigev_value.sival_ptr = &timerid;
  se.sigev_notify_function = timer_isr;
  se.sigev_notify_attributes = NULL;
  spec.it_value.tv_sec = ns / 1000000000;
  spec.it_value.tv_nsec = (ns % 1000000000) ;
  spec.it_interval.tv_sec = 0;
  spec.it_interval.tv_nsec = 0;
  timer_create (CLOCK_REALTIME, &se, &timerid);
  timer_settime (timerid, 0, &spec, NULL);
 
  
}

/* Función auxiliar para representar chars en formato binario para imprimirlos por pantalla*/
const char *byte_to_binary(char x)
{
    static char b[8];
    b[0] = '\0';

    char z;
    for (z = 0x80; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

/* Funciones que activan las transiciones */

static int fin_pantalla (fsm_t* this) 
{ 
	

	return (columna >=30); 
}

static void barrera_isr (void) 
{ 
    printf("r_barrera_isr\n");
	barrera = 1; 
}

static int barrera_infr (fsm_t* this) 
{
	return barrera; 
}

static int timer_finished (fsm_t* this) 
{ 
	return timer_led; 
}


static void clear_columna (fsm_t* this)
{
	columna=0;
	fin_representado=1;
	
}

static void change_LEDS (fsm_t* this)
{

    
	timer_led = 0;
	fin_representado=0;
	
	
	int ventana_tiempo;
	char leds=pantalla[columna];
	printf("%s\n", byte_to_binary(leds));
	
	digitalWrite(GPIO_LED_1,((leds&0x01)>>0));
	digitalWrite(GPIO_LED_2,((leds&0x02)>>1));
	digitalWrite(GPIO_LED_3,((leds&0x04)>>2));
	digitalWrite(GPIO_LED_4,((leds&0x08)>>3));
	digitalWrite(GPIO_LED_5,((leds&0x10)>>4));
	digitalWrite(GPIO_LED_6,((leds&0x20)>>5));
	digitalWrite(GPIO_LED_7,((leds&0x40)>>6));
	digitalWrite(GPIO_LED_8,((leds&0x80)>>7));
	ventana_tiempo=(int) (1000000000/(18*30));
	timer_start(ventana_tiempo);
	columna++;

}


/* Definición de estados*/
enum representar_state {
  WAITING,
  SHOW_LEDS,
};

/* Tabla de transiciónes */
static fsm_trans_t representar_tt[] = {
  { WAITING, barrera_infr, SHOW_LEDS, change_LEDS},
  { SHOW_LEDS, fin_pantalla , WAITING, clear_columna},
  { SHOW_LEDS, timer_finished , SHOW_LEDS, change_LEDS},
  {-1, NULL, -1, NULL },
};

/*Funciones para la creación e inicio de la fsm*/ 

fsm_t*
fsm_representar_new (void)
{
  fsm_t* this = (fsm_t*) malloc (sizeof (fsm_t));
  fsm_representar_init (this);
  return this;
}

void
fsm_representar_init (fsm_t* this)
{
  this->tt = representar_tt;
  this->current_state = representar_tt[0].orig_state;
  
  wiringPiSetupGpio();
  pinMode (GPIO_IR, INPUT);
  wiringPiISR (GPIO_IR, INT_EDGE_RISING, barrera_isr); 
  pinMode (GPIO_LED_1, OUTPUT);
  pinMode (GPIO_LED_2, OUTPUT);
  pinMode (GPIO_LED_3, OUTPUT);
  pinMode (GPIO_LED_4, OUTPUT);
  pinMode (GPIO_LED_5, OUTPUT);
  pinMode (GPIO_LED_6, OUTPUT);
  pinMode (GPIO_LED_7, OUTPUT);
  pinMode (GPIO_LED_8, OUTPUT);
  
  digitalWrite(GPIO_LED_1, LOW);
  digitalWrite(GPIO_LED_2, LOW);
  digitalWrite(GPIO_LED_3, LOW);
  digitalWrite(GPIO_LED_4, LOW);
  digitalWrite(GPIO_LED_5, LOW);
  digitalWrite(GPIO_LED_6, LOW);
  digitalWrite(GPIO_LED_7, LOW);
  digitalWrite(GPIO_LED_8, LOW);

}

