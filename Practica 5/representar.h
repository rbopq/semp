/*
 * representar.h
 *
 *  Created on: Ene 1, 2017
 *  Author: Rodolfo B. Oporto Quisbert / Yousra Hasnaoui
 */

#ifndef REPRESENTAR_H_
#define REPRESENTAR_H_

fsm_t* fsm_representar_new (void);
void fsm_representar_init (fsm_t* this);

#endif /* REPRESENTAR_H_ */
