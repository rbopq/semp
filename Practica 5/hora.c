/*
*  hora.c
*
*  Created on: 		Ene 20, 2017
*  Authors: 		Rodolfo B. Oporto Quisbert/ Yousra Hasnaoui
*  Description: 	Este fichero implementa las funciones necesarias de la máquina de estados
*					hora para la Práctica 5 de SEMP-MUISE 2016
*/

#include "fsm.h"
#include "representar.h"
#include "caracteres.h"


#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/time.h>
#include <wiringPi.h>
#include <time.h>
#include <unistd.h>


/* Variable compartida que representa la pantalla de LED's a representar */
extern char pantalla[30]; 

/* Variable compartida que representa el fin del proceso de representación para que se comience la otra tarea */
extern int fin_representado; 

/* Variable que representa la barrera de infrarrojos */
extern int barrera;

/*Variables para la representación de la hora*/
char h1='0';
char h0='1';
char m1='2';
char m0='3';

/* Definición de estados*/ 
enum representar_state {
  IDLE,
  GET_TIME,
};


/* Funciones que activan las transiciones, el 27 representa el número de columnas que hemos dividido la representación de la hora, representando H1H0:M1M0, 6 columnas para cada digito y 3 para los dos puntos  */
static int representado (fsm_t* this) 
{ 
	return fin_representado; 
}
 
static int true_x(fsm_t* this) 
{ 
	return 1; 
}


/* Funciones que realizan las acciones */
static void hora_to_array (fsm_t* this)
{
    barrera=0;
	char array_time[5]; // Variable auxiliar para obtener la hora en string
    array_time[5]='\0';
	
	/* Obtención de la hora*/
	time_t time_now;
    time (&time_now);
    struct tm  *timeinfo = localtime (&time_now);
	strftime(array_time, 5, "%H%M", timeinfo);
	
	/*Obtención de los caracteres que conforman la hora en formato <h1><h0>:<m1><m0>*/
	
	h1=(char)array_time[0];
	h0=(char)array_time[1];
	m1=(char)array_time[2];
	m0=(char)array_time[3];
	
	
	time_to_array(pantalla, h1, h0, m1, m0);
	
}


/* Tabla de transiciones */
static fsm_trans_t hora_tt[] = {
  { IDLE, representado, GET_TIME, NULL},
  { GET_TIME,  true_x, IDLE,  hora_to_array},
  {-1, NULL, -1, NULL },
};

/*Funciones para la creación e inicio de la fsm*/ 

fsm_t*
fsm_hora_new (void)
{
  fsm_t* this = (fsm_t*) malloc (sizeof (fsm_t));
  fsm_init (this, hora_tt);
  return this;
}

void
fsm_hora_init (fsm_t* this)
{
  this->tt = hora_tt;
  this->current_state = hora_tt[0].orig_state;

}

