/*
 * hora.h
 *
 *  Created on: Ene 1, 2017
 *  Author: Rodolfo B. Oporto Quisbert / Yousra Hasnaoui
 */

#ifndef HORA_H_
#define HORA_H_

fsm_t* fsm_hora_new (void);
void fsm_hora_init (fsm_t* this);

#endif /* HORA_H_ */
