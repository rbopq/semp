/*
* caracteres.h
*
*  Created on: 		Ene 20, 2017
*  Authors: 		Rodolfo B. Oporto Quisbert/ Yousra Hasnaoui
*  Description: 	Este fichero implementa las funciones necesarias  para la manipulación de carracteres 
*					para su representación en un array de LED para la práctica de SEMP 2016
*/

#ifndef CARACTERES_H_
#define CARACTERES_H_

/*
*	Function: 		get_caracter
*	Description:	Esta función devuelve un array que representa un caracter de 8x6 dado
*					un caracter
*	Parameters:		caracter	- Caracter a representar
*	Return:			Puntero al array que representa el caracter en formato 8x6
*/
char* get_caracter (char character);

/*
*	Function: 		time_to_array
*	Description:	Esta función escribe la hora con formato <h1><h0>:<m1><m0>
*					en un array "pantalla" que representa una pantalla de 5 caracteres
*	Parameters:		pantalla	- Puntero al array que representa la pantalla
*					h1			- Dígito más significativo de la hora
*					h0			- Dígito menos significativo de la hora
*					m1			- Dígito más significativo de los minutos
*					m0			- Dígito menos significativo de los minutos
*	Return:			void
*/
void time_to_array(char *pantalla, char h1, char h0, char m1, char m0);

#endif /* CARACTERES_H_ */
