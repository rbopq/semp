/*
*  main.c
*
*  Created on: 		Ene 20, 2017
*  Authors: 		Rodolfo B. Oporto Quisbert/ Yousra Hasnaoui
*  Description: 	Este fichero implementa la funcionalidad de la Práctica 5 de SEMP-MUISE 2017
*					La planificación sigue un esquema de ejecutivo cíclico de dos tareas. 
*/

#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include <wiringPi.h>
#include "fsm.h"
#include "representar.h"
#include "hora.h"
#include "tiempo.h"

/* Variable compartida que representa la pantalla de LED's a representar */
char pantalla[30]; 

/* Variable compartida que representa el fin del proceso de representación para que se comience la otra tarea */
int fin_representado=0; 

/* Variable que representa la barrera de infrarrojos */
int barrera =0;

extern int timer_led;

int main ()
{
  struct timespec clk_period = { 0, (1/9) * 1000000000 }; // Periodo fijado por la frecuencia de oscilación del dispositivo
  struct timespec next_activation;
  fsm_t* representar_fsm = fsm_representar_new ();
  fsm_t* hora_fsm = fsm_hora_new ();

  
  clock_gettime(CLOCK_REALTIME, &next_activation);
  
  while (1){
		clock_gettime(CLOCK_REALTIME, &next_activation);
		switch(0)
		{
			case 0: // La planificación sólo consta de un periodo secundario
				fsm_fire (representar_fsm);
				fsm_fire (hora_fsm);
				break;
			default:
				break;
		}
	    timeval_add (&next_activation, &next_activation, &clk_period);
		delay_until (&next_activation);
	}
  
  
  return 0;
  

}


