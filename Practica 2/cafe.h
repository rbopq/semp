/*
 * monedero.h
 *
 *  Created on: Dec 1, 2016
 *  Author: Rodolfo B. Oporto Quisbert
 */

#ifndef CAFE_H_
#define CAFE_H_

fsm_t* fsm_cafe_new (void);
void fsm_cafe_init (fsm_t* this);

#endif /* CAFE_H_ */
