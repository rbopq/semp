/*
 * fsm.h
 *
 *  Created on: Oct 20, 2016
 *      Author: JMMoya
 */

#ifndef TIEMPO_H_
#define TIEMPO_H_

void timer_start (int ms);
void timeval_sub (struct timespec *res, struct timespec *a, struct timespec *b);
void timeval_add (struct timespec *res, struct timespec *a, struct timespec *b);
void delay_until (struct timespec* next_activation);


#endif /* TIEMPO_H_ */
