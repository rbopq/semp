/*
 * main.c
 *
 *  Created on: Oct 20, 2016
 *  Author: Rodolfo Boris Oporto Quisbert / Yousra Hasnaoui (basado en ejemplo de JMMoya)
 */

#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include <wiringPi.h>
#include "fsm.h"
#include "cafe.h"
#include "monedero.h"
#include "tiempo.h"


int button =0;
int timer =0;
int dinero_int=0;
int dinero_val=0;

int main ()
{
  struct timespec clk_period = { 0, 250 * 1000000 };
  struct timespec next_activation;
  fsm_t* cofm_fsm = fsm_cafe_new ();
  fsm_t* monedero_fsm = fsm_monedero_new ();

	while (1){
	
		while(button ==0)
		{
			scanf("%d %d %d", &dinero_int, &dinero_val,&button); 
		}
				
	
		clock_gettime(CLOCK_REALTIME, &next_activation); 

		switch(0)
		{
			case 0:
				fsm_fire (cofm_fsm);
				fsm_fire (monedero_fsm);
			break;
			default:
				break;
		}
		timeval_add (&next_activation, &next_activation, &clk_period);
		delay_until (&next_activation);

	}
  
  return 0;
  

}
