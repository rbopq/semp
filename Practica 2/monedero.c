/*
 * fsm.c
 *
 *  Created on: Oct 20, 2016
 *      Author: JMMoya
 */
#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <signal.h>

#include "fsm.h"
#include "monedero.h"

extern pthread_mutex_t mutex_dinero;


extern int button;
extern int timer;
extern int dinero_int;
extern int dinero_val;

int dinero_acc = 0;
int dinero_dev = 0;
int precio_cafe = 50;



enum monedero_state {
  monedero_DINERO,
  monedero_DEVOLVER,
  monedero_ACUMULAR
  
};

static void devolver (fsm_t* this)
{
	dinero_dev = dinero_acc - precio_cafe;
	printf("Dinero devuelto = %d \n", dinero_dev);
	printf("Dinero = %d \n", dinero_acc);
}

static void acumular(fsm_t* this) {
    dinero_acc = dinero_acc + dinero_val;
	printf("Dinero = %d \n", dinero_acc);
}

static void acumulado(fsm_t* this) {
   dinero_val=0;
   dinero_int=0;
   printf("Dinero = %d \n", dinero_acc);
}

static void devuelto(fsm_t* this) {
   dinero_dev=0;
   dinero_acc=0;
   printf("Dinero = %d \n", dinero_acc);
}

static int hay_moneda_nueva(fsm_t* this) {
  return dinero_int;
}
 

static int pulsar_boton_y_dinero_suficiente (fsm_t* this) 
{ 
	return (button && (dinero_acc >= precio_cafe)); 
}

static int sin_entrada (fsm_t* this) 
{ 
	return 1; 
}

static fsm_trans_t monedero[] = {
  { monedero_DINERO, pulsar_boton_y_dinero_suficiente, monedero_DEVOLVER, devolver },
  { monedero_DEVOLVER, sin_entrada, monedero_DINERO, devuelto },
  { monedero_DINERO, hay_moneda_nueva, monedero_ACUMULAR, acumular },
  { monedero_ACUMULAR, sin_entrada, monedero_DINERO, acumulado },  
  {-1, NULL, -1, NULL },
};

fsm_t*
fsm_monedero_new (void)
{
  fsm_t* this = (fsm_t*) malloc (sizeof (fsm_t));
  fsm_monedero_init (this);
  return this;
}

void
fsm_monedero_init (fsm_t* this)
{
  this->tt = monedero;
  this->current_state = monedero[0].orig_state;
  
}





