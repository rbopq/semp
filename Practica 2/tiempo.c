#include <stdio.h>
#include <sys/time.h>
#include <sys/select.h>
#include <time.h>
#include <signal.h>
#include "tiempo.h"


extern int timer;

static void timer_isr (union sigval arg) 
{ 
	printf("timer_isr\n");
	timer = 1; 
}

void timer_start (int ms)
{
  timer_t timerid;
  struct itimerspec spec;
  struct sigevent se;
  se.sigev_notify = SIGEV_THREAD;
  se.sigev_value.sival_ptr = &timerid;
  se.sigev_notify_function = timer_isr;
  se.sigev_notify_attributes = NULL;
  spec.it_value.tv_sec = ms / 1000;
  spec.it_value.tv_nsec = (ms % 1000) * 1000000;
  spec.it_interval.tv_sec = 0;
  spec.it_interval.tv_nsec = 0;
  timer_create (CLOCK_REALTIME, &se, &timerid);
  timer_settime (timerid, 0, &spec, NULL);
  timer=0;
  
}

// Utility functions, should be elsewhere

// res = a - b
void
timeval_sub (struct timespec *res, struct timespec  *a, struct timespec  *b)
{
  res->tv_sec = a->tv_sec - b->tv_sec;
  res->tv_nsec = a->tv_nsec - b->tv_nsec;
  if (res->tv_nsec < 0) {
    --res->tv_sec;
    res->tv_nsec += 1000000000;
  }
}

// res = a + b
void
timeval_add (struct timespec  *res, struct timespec  *a, struct timespec  *b)
{
  res->tv_sec = a->tv_sec + b->tv_sec
    + a->tv_nsec / 1000000000 + b->tv_nsec / 1000000000; 
  res->tv_nsec = a->tv_nsec % 1000000000 + b->tv_nsec % 1000000000;
}

// wait until next_activation (absolute time)
void delay_until (struct timespec * next_activation)
{
  struct timespec now, timeout;
  clock_gettime(CLOCK_REALTIME, &now);
  timeval_sub (&timeout, next_activation, &now);
  pselect (0, NULL, NULL, NULL, &timeout, NULL);
}

