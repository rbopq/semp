#!/bin/sh

sudo apt-get install -y libncurses5-dev gcc make git exuberant-ctags bc libssl-dev sshpass

if "x$RASPADDR" == "x"; then
  echo "Setup RASPADDR env variable to raspberry IP address"
  echo "Example: export RASPADDR=192.168.1.18"
  exit 1
fi

if "x$PASS" == "x"; then
  echo "Setup PASS env variable to raspberry password for user pi"
  echo "Example: export PASS=raspberry"
  exit 2
fi

mkdir raspberry
cd raspberry

# Create source files for Cross compiling
echo 'export PASS=$PASS' > raspi.comm.rc
echo 'export RASPADDR=$RASPADDR' >> raspi.comm.rc
echo 'export PATH=/opt/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin:$PATH' >> raspi.comm.rc
echo 'export RASPI_REPOS_DIR=/home/alumno/raspberry' >> raspi.comm.rc
echo 'export INSTALL_MOD_PATH=${RASPI_REPOS_DIR}/modules' >> raspi.comm.rc
echo 'export KERNEL_SRC=${RASPI_REPOS_DIR}/linux' >> raspi.comm.rc
#echo 'export CCPREFIX=arm-linux-gnueabihf-' >> raspi.comm.rc
echo 'export CCPREFIX=/home/raspberry/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin/arm-linux-gnueabihf-' >> raspi.comm.rc
echo '' >> raspi.comm.rc
echo 'alias rpi-make="ARCH=arm CROSS_COMPILE=$CCPREFIX make"' >> raspi.comm.rc
echo 'alias rpi-config="rpi-make $DEFCONFIG"' >> raspi.comm.rc
echo 'alias rpi-driver-make="rpi-make -C ${KERNEL_SRC}"' >> raspi.comm.rc
echo 'alias rpi-ssh="sshpass -p ${PASS} ssh pi@$RASPADDR"' >> raspi.comm.rc
echo 'rpi-cp () { sshpass -p ${PASS} scp "$@" pi@"$RASPADDR":; }' >> raspi.comm.rc
echo 'rpi-bring () { sshpass -p ${PASS} scp pi@"$RASPADDR":$@; }' >> raspi.comm.rc

echo 'export KERNEL=kernel' > raspi.rc
echo 'export DEFCONFIG=bcmrpi_defconfig' >> raspi.rc

echo 'export KERNEL=kernel7' > raspi2.rc
echo 'export DEFCONFIG=bcm2708_defconfig' >> raspi2.rc

source raspi.rc

RES=`rpi-ssh 'pwd'`
if "x$RES" != "x/home/pi"; then
  echo "No connection to raspberry at $RASPADDR"
  exit 3
fi

# Check for wiringPi at raspberry. Install if not present
RES=`rpi-ssh 'ls /usr/local/lib/libwiringPi.so'`
if "x$RES" == "x"; then
  rpi-ssh 'git clone git://git.drogon.net/wiringPi && cd wiringPi && ./build'
  RES=`rpi-ssh 'ls /usr/local/lib/libwiringPi.so'`
  if "x$RES" == "x"; then
    echo "Error installing wiringPi at raspberry. Do it manually"
    exit 4
  fi
fi

# Bring files from wiringPi in raspberry to Host for cross-compiling
mkdir -p include/wiringPi
rpi-bring /usr/local/include/* include/wiringPi

mkdir -p lib
rpi-bring /usr/local/lib/libwiringPi*.so.* lib
cd lib
ln -s libwiringPiDev.so.* libwiringPiDev.so
ln -s libwiringPi.so.* libwiringPi.so
cd ..

# Native or cross compilation:
# https://www.raspberrypi.org/documentation/linux/kernel/building.md
# Cross Toolchain
git clone https://github.com/raspberrypi/tools
sudo cp -r tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64 /opt

# Linux kernel for Raspberry
git clone --depth=1 https://github.com/raspberrypi/linux

# Virtual machine for raspberry with Linux Kernel: read repo README.md
git clone https://github.com/adafruit/Adafruit-Pi-Kernel-o-Matic Kernel-o-Matic

# Configure kernel with default configuration for raspi
cd linux
rpi-config

# Modify default module configuration
# rpi-make menuconfig
# vi .config

# Cross-Compile kernel
rpi-make zImage modules dtbs

# Alternatively, copy configuration from your raspi if available
# zcat /proc/config.gz .config
# scp /boot/config-<kernel-version> .config
mkdir -p $INSTALL_MOD_PATH
rpi-driver-make INSTALL_MOD_PATH=/home/alumno/raspberry/modules modules modules_install | grep DEPMOD | awk '{print $2}'
RPI_KERNEL=`ls $INSTALL_MOD_PATH/lib/modules/`
sshpass -p $PASS rsync -avP $INSTALL_MOD_PATH/lib/modules/* pi@$RASPADDR:
rpi-ssh 'sudo mv $RPI_KERNEL /lib/modules'

# Copy kernel to raspi
rpi-cp arch/arm/boot/zImage
rpi-ssh 'if ! test -f /boot/kernel.img.orig; then sudo cp /boot/kernel.img /boot/kernel.img.orig; fi; sudo cp /home/pi/zImage kernel.img;'

